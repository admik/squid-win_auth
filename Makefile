PORTNAME=	squid-win_auth
PORTVERSION=	0.1

dummy:

distfile:
	@/bin/cp -R ${PORTNAME} ${PORTNAME}-${PORTVERSION}
	@/usr/bin/tar czf ${PORTNAME}-${PORTVERSION}.tar.gz ${PORTNAME}-${PORTVERSION} 
	@/bin/rm -R ${PORTNAME}-${PORTVERSION}
	@/sbin/md5 ${PORTNAME}-${PORTVERSION}.tar.gz > port/distinfo
	@/sbin/sha256 ${PORTNAME}-${PORTVERSION}.tar.gz >> port/distinfo 
	@/usr/bin/stat -f "SIZE (%N) = %z" ${PORTNAME}-${PORTVERSION}.tar.gz >> port/distinfo
